package toyfactory;

public abstract class Toy {
    private Double basePrice;
    protected ToySize size;

    public Toy(double basePrice, ToySize size) {
        this.basePrice = basePrice;
        this.size = size;
    }

    public Double getPrice() {
        switch (size) {
            case SMALL:
                return basePrice;
            case MEDIUM:
                return 1.5 * basePrice;
            case LARGE:
                return 2 * basePrice;
            default:
                return null;
        }
    }

    public Double getPrice(Double discount) {
        if (discount > 0D && discount < 100D)
            return getPrice() - ((getPrice() * discount) / 100);
        return null;
    }

    public Double getBasePrice() {
        return basePrice;
    }
}
